extern crate websocket;
extern crate serde_cbor;

use serde_cbor::{from_slice, Value, value};

use handler;
use message;

use std::collections::HashMap;

pub fn cbor(raw: websocket::OwnedMessage) -> Option<Result<Vec<u8>, serde_cbor::Error>> {

    match raw {
        websocket::OwnedMessage::Binary(payload) => {

            let value: Value = from_slice(payload.as_slice()).unwrap();

            let obj = match value.as_object() {
                Some(d) => d.clone(),
                None => {
                    let mut ret = HashMap::new();
                    ret.insert(value::ObjectKey::String("error".to_string()),
                               Value::String("error".to_string()));
                    ret
                }
            };

            let message = message::SkaroMessage(&obj);

            match message.get_str("type").unwrap() {
                "request" => handler::__request__(message).ok(),
                "changed_user_data" => {
                    handler::changed_user_data::__on_changed_user_data(message);
                    None
                }
                "received_message" => {
                    handler::received_message::__on_received_message(message);
                    None
                }
                "error" => {
                    handler::error::__on_error(message);
                    None
                }
                "info" => {
                    handler::info::__on_info(message);
                    None
                }
                _ => {
                    println!("Error! Key 'type' not found");
                    None
                }
            }

        }
        _ => {
            match true {
                _ => None,
            }
        }
    }

}
