use handler::*;

pub fn search_contacts(query: &str) -> Result<Vec<u8>, serde_cbor::Error> {
    let mut storage = STORAGE.lock().unwrap();
    let mut storage_data: HashMap<String, String> = HashMap::new();
    storage_data.insert("type".to_string(), "search_contacts".to_string());

    let mut response = HashMap::new();
    let uuid = Uuid::new_v4().hyphenated().to_string();
    response.insert("chain_uuid", Value::String(uuid.clone()));
    response.insert("type", Value::String("search_contacts".to_string()));
    response.insert("query", Value::String(query.to_string()));

    storage.insert(uuid, storage_data);
    serde_cbor::to_vec(&response)
}

pub fn __on_search_contacts(message: message::SkaroMessage) {
    let status = message.get_str("status").unwrap();

    let data = if status == "200 OK" {
        message.get_arr("result").and_then(|s| s.to_json()).unwrap()
    } else {
        "".to_string()
    };

    let c_status = CString::new(status).unwrap();
    let c_info = CString::new(message.get_str("info").unwrap_or("")).unwrap();
    let c_data = CString::new(data).unwrap();

    unsafe {
        skaro_on_search_contacts(c_status.as_ptr(), c_info.as_ptr(), c_data.as_ptr());
    }
}
