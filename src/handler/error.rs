use handler::*;

pub fn __on_error(message: message::SkaroMessage) {

    let c_status = CString::new(message.get_str("status").unwrap_or("")).unwrap();
    let c_info = CString::new(message.get_str("info").unwrap_or("")).unwrap();

    unsafe {
        skaro_on_error(c_status.as_ptr(), c_info.as_ptr());
    }
}
