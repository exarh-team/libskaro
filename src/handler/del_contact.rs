use handler::*;

pub fn del_contact(login: &str) -> Result<Vec<u8>, serde_cbor::Error> {
    let mut storage = STORAGE.lock().unwrap();
    let mut storage_data: HashMap<String, String> = HashMap::new();
    storage_data.insert("type".to_string(), "del_contact".to_string());
    storage_data.insert("login".to_string(), login.to_string());

    let mut response = HashMap::new();
    let uuid = Uuid::new_v4().hyphenated().to_string();
    response.insert("chain_uuid", Value::String(uuid.clone()));
    response.insert("type", Value::String("del_contact".to_string()));
    response.insert("login", Value::String(login.to_string()));

    storage.insert(uuid, storage_data);
    serde_cbor::to_vec(&response)
}

pub fn __on_del_contact(message: message::SkaroMessage, data: &HashMap<String, String>) {
    let status = message.get_str("status").unwrap();
    let login = data.get("login").unwrap_or(&"".to_string()).to_owned();

    let c_status = CString::new(status).unwrap();
    let c_info = CString::new(message.get_str("info").unwrap_or("")).unwrap();
    let c_login = CString::new(login).unwrap();

    unsafe {
        skaro_on_del_contact(c_status.as_ptr(), c_info.as_ptr(), c_login.as_ptr());
    }
}
