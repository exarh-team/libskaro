use handler::*;

pub fn __on_received_message(message: message::SkaroMessage) {
    let from = message.get_obj("from").and_then(|s| s.to_json()).unwrap();

    let c_mid = message.get_int("mid").unwrap_or(0);
    let c_date = CString::new(message.get_str("date").unwrap_or("")).unwrap();
    let c_from = CString::new(from).unwrap();
    let c_body = CString::new(message.get_str("body").unwrap_or("")).unwrap();

    unsafe {
        skaro_on_received_message(c_mid, c_date.as_ptr(), c_from.as_ptr(), c_body.as_ptr());
    }
}
