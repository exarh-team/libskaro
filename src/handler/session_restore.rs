use handler::*;

pub fn session_restore(session_id: &str) -> Result<Vec<u8>, serde_cbor::Error> {
    let mut storage = STORAGE.lock().unwrap();
    let mut storage_data: HashMap<String, String> = HashMap::new();
    storage_data.insert("type".to_string(), "session_restore".to_string());

    let mut response = HashMap::new();
    let uuid = Uuid::new_v4().hyphenated().to_string();
    response.insert("chain_uuid", Value::String(uuid.clone()));
    response.insert("type", Value::String("session_restore".to_string()));
    response.insert("session_id", Value::String(session_id.to_string()));

    storage.insert(uuid, storage_data);
    serde_cbor::to_vec(&response)
}

pub fn __on_session_restore(message: message::SkaroMessage) {
    let status = message.get_str("status").unwrap();

    let data = if status == "200 OK" {
        message.get_obj("user").and_then(|s| s.to_json()).unwrap()
    } else {
        "".to_string()
    };

    let c_status = CString::new(status).unwrap();
    let c_info = CString::new(message.get_str("info").unwrap_or("")).unwrap();
    let c_session_id = CString::new(message.get_str("session_id").unwrap_or("")).unwrap();
    let c_data = CString::new(data).unwrap();

    unsafe {
        skaro_on_session_restore(c_status.as_ptr(),
                                 c_info.as_ptr(),
                                 c_session_id.as_ptr(),
                                 c_data.as_ptr());
    }
}
