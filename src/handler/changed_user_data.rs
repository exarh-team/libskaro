use handler::*;

pub fn __on_changed_user_data(message: message::SkaroMessage) {
    let data = message.get_obj("update").and_then(|s| s.to_json()).unwrap();

    let c_login = CString::new(message.get_str("login").unwrap_or("")).unwrap();
    let c_data = CString::new(data).unwrap();

    unsafe {
        skaro_on_changed_user_data(c_login.as_ptr(), c_data.as_ptr());
    }
}
