// FFI
use std::ffi::{CString, CStr};
use libc::c_char;

use std::collections::HashMap;
use std::sync::Mutex;

use serde_cbor;
use serde_cbor::Value;

use uuid::Uuid;

use message;

// Регистрация/Авторизация
pub mod authorize;
pub mod register_type;
pub mod register;
pub mod session_restore;

// Работа с контактами
pub mod get_contacts;
pub mod add_contact;
pub mod del_contact;
pub mod search_contacts;

// Работа с пользователями
pub mod get_user_data;
pub mod changed_user_data;

// Работа с сообщениями
pub mod get_history;
pub mod send_message;
pub mod received_message;

// Работа со своими данными
pub mod update_my_data;

// Прочее
pub mod error;
pub mod info;

extern "C" {

    // Регистрация/Авторизация

    /// Called when keys were generated.
    ///
    /// ## C header
    ///
    /// ```ignore
    /// void export_keys(const char* login, const char* public_key, const char* private_key);
    /// ```
    pub fn skaro_export_keys(login: *const c_char,
                             public_key: *const c_char,
                             private_key: *const c_char);

    /// The 'data' parameter contains data about the current user in json format.
    ///
    /// ## C header
    ///
    /// ```ignore
    /// void skaro_on_authorize
    /// (const char* status, const char* info, const char* session_id, const char* data);
    /// ```
    pub fn skaro_on_authorize(status: *const c_char,
                              info: *const c_char,
                              session_id: *const c_char,
                              data: *const c_char);

    /// ## C header
    ///
    /// ```ignore
    /// void skaro_on_register_type
    /// (const char* status, const char* info, const char* register_type);
    /// ```
    pub fn skaro_on_register_type(status: *const c_char,
                                  info: *const c_char,
                                  register_type: *const c_char);

    /// ## C header
    ///
    /// ```ignore
    /// void skaro_on_register(const char* status, const char* info);
    /// ```
    pub fn skaro_on_register(status: *const c_char, info: *const c_char);

    /// The 'data' parameter contains data about the current user in json format.
    ///
    /// ## C header
    ///
    /// ```ignore
    /// void skaro_on_session_restore
    /// (const char* status, const char* info, const char* session_id, const char* data);
    /// ```
    pub fn skaro_on_session_restore(status: *const c_char,
                                    info: *const c_char,
                                    session_id: *const c_char,
                                    data: *const c_char);


    // Работа с контактами

    /// The 'data' parameter contains data about the current user in json format.
    ///
    /// ## C header
    ///
    /// ```ignore
    /// void skaro_on_get_contacts(const char* status, const char* info, const char* data);
    /// ```
    pub fn skaro_on_get_contacts(status: *const c_char, info: *const c_char, data: *const c_char);

    /// ## C header
    ///
    /// ```ignore
    /// void skaro_on_add_contact(const char* status, const char* info, const char* data);
    /// ```
    pub fn skaro_on_add_contact(status: *const c_char, info: *const c_char, data: *const c_char);

    /// ## C header
    ///
    /// ```ignore
    /// void skaro_on_del_contact(const char* status, const char* info, login: *const c_char);
    /// ```
    pub fn skaro_on_del_contact(status: *const c_char, info: *const c_char, login: *const c_char);

    /// The 'data' parameter contains data about the current user in json format.
    ///
    /// ## C header
    ///
    /// ```ignore
    /// void skaro_on_search_contacts(const char* status, const char* info, const char* data);
    /// ```
    pub fn skaro_on_search_contacts(status: *const c_char,
                                    info: *const c_char,
                                    data: *const c_char);


    // Работа с пользователями

    /// The 'data' parameter contains data about the current user in json format.
    ///
    /// ## C header
    ///
    /// ```ignore
    /// void skaro_on_get_user_data(const char* status, const char* info, const char* data);
    /// ```
    pub fn skaro_on_get_user_data(status: *const c_char,
                                  info: *const c_char,
                                  data: *const c_char);

    /// The 'data' parameter contains data about the current user in json format.
    ///
    /// ## C header
    ///
    /// ```ignore
    /// void skaro_on_changed_user_data(const char* login, const char* data);
    /// ```
    pub fn skaro_on_changed_user_data(login: *const c_char, data: *const c_char);

    // Работа с сообщениями

    /// The data parameter contains data about the current user in json format.
    ///
    /// ## C header
    ///
    /// ```ignore
    /// void skaro_on_get_history
    /// (const char* status, const char* info, const char* login, const char* data);
    /// ```
    pub fn skaro_on_get_history(status: *const c_char,
                                info: *const c_char,
                                login: *const c_char,
                                data: *const c_char);

    /// ## C header
    ///
    /// ```ignore
    /// void skaro_on_send_message
    /// (const char* status, const char* info, int mid,
    /// const char* date, const char* login, const char* marker);
    /// ```
    pub fn skaro_on_send_message(status: *const c_char,
                                 info: *const c_char,
                                 mid: i32,
                                 date: *const c_char,
                                 login: *const c_char,
                                 marker: *const c_char);

    /// The 'from' parameter contains data about the current user in json format.
    ///
    /// ## C header
    ///
    /// ```ignore
    /// void skaro_on_received_message
    /// (int mid, const char* date, const char* from, const char* body);
    /// ```
    pub fn skaro_on_received_message(mid: i32,
                                     date: *const c_char,
                                     from: *const c_char,
                                     body: *const c_char);


    // Работа со своими данными

    /// ## C header
    ///
    /// ```ignore
    /// void skaro_on_update_my_data(const char* status, const char* info, const char* records);
    /// ```
    pub fn skaro_on_update_my_data(status: *const c_char,
                                   info: *const c_char,
                                   records: *const c_char);


    // Прочее

    /// ## C header
    ///
    /// ```ignore
    /// void skaro_on_error(const char* status, const char* info);
    /// ```
    pub fn skaro_on_error(status: *const c_char, info: *const c_char);

    /// ## C header
    ///
    /// ```ignore
    /// void skaro_on_info(const char* status, const char* info);
    /// ```
    pub fn skaro_on_info(status: *const c_char, info: *const c_char);
}

lazy_static! {
    pub static ref STORAGE: Mutex<HashMap<String, HashMap<String, String>>> = {
          let map = HashMap::new();
          Mutex::new(map)
    };
}

pub fn __request__(message: message::SkaroMessage)
                   -> Result<Result<Vec<u8>, serde_cbor::Error>, &'static str> {
    let storage = {
        let store = STORAGE.lock().unwrap();
        store.clone()
    };

    let uuid = message.get_str("chain_uuid").unwrap();

    match storage.get(uuid) {
        Some(data) => {

            {
                let mut storage = STORAGE.lock().unwrap();
                storage.remove(uuid);
            }

            match data.get("type") {
                Some(s) => {
                    match s.as_str() {
                        // Регистрация/Авторизация
                        "authorize" => Ok(authorize::__on_authorize(message, data)),
                        "authorize2" => {
                            authorize::__on_authorize_2(message);
                            Err("")
                        }
                        "register_type" => {
                            register_type::__on_register_type(message);
                            Err("")
                        }
                        "register" => {
                            register::__on_register(message, data);
                            Err("")
                        }
                        "session_restore" => {
                            session_restore::__on_session_restore(message);
                            Err("")
                        }
                        // Работа с контактами
                        "get_contacts" => {
                            get_contacts::__on_get_contacts(message);
                            Err("")
                        }
                        "add_contact" => {
                            add_contact::__on_add_contact(message);
                            Err("")
                        }
                        "del_contact" => {
                            del_contact::__on_del_contact(message, data);
                            Err("")
                        }
                        "search_contacts" => {
                            search_contacts::__on_search_contacts(message);
                            Err("")
                        }
                        // Работа с пользователями
                        "get_user_data" => {
                            get_user_data::__on_get_user_data(message);
                            Err("")
                        }
                        // Работа с сообщениями
                        "get_history" => {
                            get_history::__on_get_history(message, data);
                            Err("")
                        }
                        "send_message" => {
                            send_message::__on_send_message(message, data);
                            Err("")
                        }
                        // Работа со своими данными
                        "update_my_data" => {
                            update_my_data::__on_update_my_data(message, data);
                            Err("")
                        }
                        &_ => Err("ошибка обработки цепочки"),
                    }
                }
                None => Err("цепочка не найдена в хранилище"),
            }
        }
        None => Err("цепочка не найдена в хранилище"),
    }
}

fn storage_insert(key: String, value: HashMap<String, String>) {
    let mut storage = STORAGE.lock().unwrap();
    storage.insert(key, value);
}
