use serde_json;
use handler::*;

pub fn update_my_data(records: &str) -> Result<Vec<u8>, serde_cbor::Error> {
    let mut storage = STORAGE.lock().unwrap();
    let mut storage_data: HashMap<String, String> = HashMap::new();
    storage_data.insert("type".to_string(), "update_my_data".to_string());
    storage_data.insert("records".to_string(), records.to_string());

    let records_json: Value = serde_json::from_str(records).unwrap();

    let mut response = HashMap::new();
    let uuid = Uuid::new_v4().hyphenated().to_string();
    response.insert("chain_uuid", Value::String(uuid.clone()));
    response.insert("type", Value::String("update_my_data".to_string()));
    response.insert("update", records_json);

    storage.insert(uuid, storage_data);
    serde_cbor::to_vec(&response)
}

pub fn __on_update_my_data(message: message::SkaroMessage, data: &HashMap<String, String>) {
    let status = message.get_str("status").unwrap();
    let records = data.get("records").unwrap_or(&"".to_string()).to_owned();

    let c_status = CString::new(status).unwrap();
    let c_info = CString::new(message.get_str("info").unwrap_or("")).unwrap();
    let c_records = CString::new(records).unwrap();

    unsafe {
        skaro_on_update_my_data(c_status.as_ptr(), c_info.as_ptr(), c_records.as_ptr());
    }
}
