// SHA256
use sha::sha256::Sha256;
use sha::utils::Digest;
use sha::utils::DigestExt;

// RSA
use openssl::rsa::Rsa;
use openssl::rsa::PKCS1_PADDING;

use base64::decode;

use handler::*;

pub fn authorize(login: &str,
                 private_key: &str,
                 password: &str)
                 -> Result<Vec<u8>, serde_cbor::Error> {
    let mut storage = STORAGE.lock().unwrap();
    let mut storage_data: HashMap<String, String> = HashMap::new();
    storage_data.insert("type".to_string(), "authorize".to_string());
    storage_data.insert("private_key".to_string(), private_key.to_string());
    storage_data.insert("password".to_string(), password.to_string());

    let mut response = HashMap::new();
    let uuid = Uuid::new_v4().hyphenated().to_string();
    response.insert("chain_uuid", Value::String(uuid.clone()));
    response.insert("type", Value::String("authorize".to_string()));
    response.insert("login", Value::String(login.to_string()));

    storage.insert(uuid, storage_data);

    serde_cbor::to_vec(&response)
}

pub fn __on_authorize(message: message::SkaroMessage,
                      data: &HashMap<String, String>)
                      -> Result<Vec<u8>, serde_cbor::Error> {
    let mut storage_data: HashMap<String, String> = HashMap::new();

    storage_data.insert("type".to_string(), "authorize2".to_string());

    let encrypted_rnd = &decode(message.get_str("encrypted_rnd").unwrap().as_bytes()).unwrap();

    let private_key =
        Rsa::private_key_from_pem_passphrase(data.get("private_key").unwrap().as_bytes(),
                                             data.get("password").unwrap().as_bytes())
            .unwrap();
    let mut dec_result = vec![0; private_key.size()];

    let len = private_key.private_decrypt(&encrypted_rnd, &mut dec_result, PKCS1_PADDING).unwrap();

    let mut h: Sha256 = Default::default();
    let res = h.digest(&dec_result[..len]).to_hex();

    let mut data = HashMap::new();
    data.insert("chain_uuid",
                Value::String(message.get_str("chain_uuid").unwrap().to_string()));
    data.insert("type", Value::String("request".to_string()));
    data.insert("rnd_hash", Value::String(res));
    data.insert("status", Value::String("200 OK".to_string()));

    storage_insert(message.get_str("chain_uuid").unwrap().to_string(),
                   storage_data);
    serde_cbor::to_vec(&data)
}

pub fn __on_authorize_2(message: message::SkaroMessage) {
    let status = message.get_str("status").unwrap();

    let data = if status == "200 OK" {
        message.get_obj("user").and_then(|s| s.to_json()).unwrap()
    } else {
        "".to_string()
    };

    let c_status = CString::new(status).unwrap();
    let c_info = CString::new(message.get_str("info").unwrap_or("")).unwrap();
    let c_session_id = CString::new(message.get_str("session_id").unwrap_or("")).unwrap();
    let c_data = CString::new(data).unwrap();

    unsafe {
        skaro_on_authorize(c_status.as_ptr(),
                           c_info.as_ptr(),
                           c_session_id.as_ptr(),
                           c_data.as_ptr());
    }
}
