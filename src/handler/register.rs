// RSA
use openssl::rsa::Rsa;
use openssl::symm::Cipher;

use handler::*;

pub fn register(email: &str, login: &str, password: &str) -> Result<Vec<u8>, serde_cbor::Error> {
    let mut storage = STORAGE.lock().unwrap();
    let mut storage_data: HashMap<String, String> = HashMap::new();
    storage_data.insert("type".to_string(), "register".to_string());
    storage_data.insert("login".to_string(), login.to_string());

    // 1. Клиент выбирает себе логин и пароль
    // 2. Клиент генерирует ключевую пару RSA.
    //    Публичный ключ e,N и приватный ключ d,N
    let key = Rsa::generate(2048).unwrap();
    let public_key = key.public_key_to_pem().unwrap();
    // 3. Клиент устанавливает пароль на приватный ключ
    let private_key = key.private_key_to_pem_passphrase(Cipher::aes_256_cbc(), password.as_bytes())
        .unwrap();

    // 4. Клиент сохраняет в локальном хранилище
    // свой публичный ключ и запароленный приватный ключ
    let public_key_string = String::from_utf8(public_key).expect("Found invalid UTF-8");
    storage_data.insert("public_key".to_string(), public_key_string.clone());
    storage_data.insert("private_key".to_string(),
                        String::from_utf8(private_key).expect("Found invalid UTF-8"));

    // 5. Клиент отсылает на сервер, а сервер сохраняет:
    // логин, E-Mail и открытый ключ e,N.
    let mut response = HashMap::new();
    let uuid = Uuid::new_v4().hyphenated().to_string();
    response.insert("chain_uuid", Value::String(uuid.clone()));
    response.insert("type", Value::String("register".to_string()));
    response.insert("login", Value::String(login.to_string()));
    response.insert("email", Value::String(email.to_string()));
    response.insert("public_key", Value::String(public_key_string));

    storage.insert(uuid, storage_data);
    serde_cbor::to_vec(&response)
}

pub fn __on_register(message: message::SkaroMessage, data: &HashMap<String, String>) {

    let status = message.get_str("status").unwrap();
    let login = data.get("login").unwrap();
    let c_login = CString::new(login.to_string()).unwrap();

    if status == "200 OK" {
        unsafe {
            skaro_export_keys(c_login.as_ptr(),
                              CStr::from_bytes_with_nul_unchecked(data.get("private_key")
                                      .unwrap()
                                      .as_bytes())
                                  .as_ptr(),
                              CStr::from_bytes_with_nul_unchecked(data.get("private_key")
                                      .unwrap()
                                      .as_bytes())
                                  .as_ptr());
        }
    }

    let c_status = CString::new(status).unwrap();
    let c_info = CString::new(message.get_str("info").unwrap_or("")).unwrap();

    unsafe {
        skaro_on_register(c_status.as_ptr(), c_info.as_ptr());
    }
}
