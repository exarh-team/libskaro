use handler::*;

pub fn send_message(to: &str,
                    body: &str,
                    is_conference: bool,
                    marker: &str)
                    -> Result<Vec<u8>, serde_cbor::Error> {
    let mut storage = STORAGE.lock().unwrap();
    let mut storage_data: HashMap<String, String> = HashMap::new();
    storage_data.insert("type".to_string(), "send_message".to_string());
    storage_data.insert("marker".to_string(), marker.to_string());
    storage_data.insert("login".to_string(), to.to_string());

    let mut response = HashMap::new();
    let uuid = Uuid::new_v4().hyphenated().to_string();
    response.insert("chain_uuid", Value::String(uuid.clone()));
    response.insert("type", Value::String("send_message".to_string()));
    response.insert("to", Value::String(to.to_string()));
    response.insert("body", Value::String(body.to_string()));
    response.insert("is_conference", Value::Bool(is_conference));

    storage.insert(uuid, storage_data);
    serde_cbor::to_vec(&response)
}

pub fn __on_send_message(message: message::SkaroMessage, data: &HashMap<String, String>) {

    let status = message.get_str("status").unwrap();
    let marker = data.get("marker").unwrap_or(&"".to_string()).to_owned();
    let login = data.get("login").unwrap_or(&"".to_string()).to_owned();

    let c_status = CString::new(status).unwrap();
    let c_info = CString::new(message.get_str("info").unwrap_or("")).unwrap();
    let c_mid = message.get_int("mid").unwrap_or(0);
    let c_date = CString::new(message.get_str("date").unwrap_or("")).unwrap();
    let c_login = CString::new(login).unwrap();
    let c_marker = CString::new(marker).unwrap();

    unsafe {
        skaro_on_send_message(c_status.as_ptr(),
                              c_info.as_ptr(),
                              c_mid,
                              c_date.as_ptr(),
                              c_login.as_ptr(),
                              c_marker.as_ptr());
    }
}
