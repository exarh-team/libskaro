use handler::*;

pub fn register_type() -> Result<Vec<u8>, serde_cbor::Error> {
    let mut storage = STORAGE.lock().unwrap();
    let mut storage_data: HashMap<String, String> = HashMap::new();
    storage_data.insert("type".to_string(), "register_type".to_string());

    let mut response = HashMap::new();
    let uuid = Uuid::new_v4().hyphenated().to_string();
    response.insert("chain_uuid", Value::String(uuid.clone()));
    response.insert("type", Value::String("register_type".to_string()));

    storage.insert(uuid, storage_data);
    serde_cbor::to_vec(&response)
}

pub fn __on_register_type(message: message::SkaroMessage) {
    let c_status = CString::new(message.get_str("status").unwrap()).unwrap();
    let c_info = CString::new(message.get_str("info").unwrap_or("")).unwrap();
    let c_register_type = CString::new(message.get_str("register_type").unwrap_or("")).unwrap();

    unsafe {
        skaro_on_register_type(c_status.as_ptr(), c_info.as_ptr(), c_register_type.as_ptr());
    }
}
