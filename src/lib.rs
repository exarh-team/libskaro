//! libskaro - a library that simplifies working with Skaro protocol.
//!
//! # What is Skaro?
//! Skaro is an open standard for instant messaging.
//! [More about skaro](https://gitlab.com/exarh-team/skaro-server/wikis/home).
//!
//!

extern crate websocket;
extern crate serde_cbor;
extern crate serde_json;
extern crate openssl;
extern crate base64;
extern crate sha;
extern crate uuid;
// C API for Skaro library
extern crate libc;
use std::ffi::CStr;
use std::os::raw::c_char;
use std::any::Any;

#[macro_use]
extern crate lazy_static;

mod dalek;
pub mod message;
pub mod handler;
mod parse;

/// The function to connect to the server.
/// Receive the server address,
/// returns a pointer to Sender, used for other API functions
///
/// ## Example (C)
/// ### Headers
///
/// ```ignore
/// typedef void * handle;
///
/// extern handle skaro_connect(const char*);
/// ```
///
/// ### Implementation
///
/// ```ignore
/// handle x;
///
/// x = skaro_connect("ws://127.0.0.1:8000/skaro_client/");
/// ```
#[no_mangle]
pub extern "C" fn skaro_connect(server: *const c_char) -> *mut () {
    dalek::ws_connect(unsafe { CStr::from_ptr(server).to_string_lossy().into_owned().as_str() })
}

/// ## Example (C)
/// ### Headers
///
/// ```ignore
/// void skaro_disconnect(handle);
/// ```
///
/// ### Implementation
///
///  `skaro_disconnect(x);`
#[no_mangle]
pub extern "C" fn skaro_disconnect(handle: *mut std::sync::mpsc::Sender<websocket::OwnedMessage>) {
    let y: Box<Any> =
        unsafe { Box::<std::sync::mpsc::Sender<websocket::OwnedMessage>>::from_raw(handle) };
    std::mem::forget(y);
}

/// ## Example (C)
/// ### Headers
///
/// ```ignore
/// void skaro_authorize(handle, const char*, const char*, const char*);
/// ```
///
/// ### Implementation
///
/// ```ignore
/// char* encrypted_private_key = ...;
///
/// skaro_authorize(x, "login", encrypted_private_key, "password");
/// ```
#[no_mangle]
pub extern "C" fn skaro_authorize(handle: *mut std::sync::mpsc::Sender<websocket::OwnedMessage>,
                                  c_login: *const c_char,
                                  c_private_key: *const c_char,
                                  c_password: *const c_char) {

    let maybe_channel = unsafe { handle.as_mut() };

    match maybe_channel {
        Some(s) => {
            let login: &str = unsafe { CStr::from_ptr(c_login) }.to_str().unwrap();
            let private_key: &str = unsafe { CStr::from_ptr(c_private_key) }.to_str().unwrap();
            let password: &str = unsafe { CStr::from_ptr(c_password) }.to_str().unwrap();

            let _ = s.send(websocket::OwnedMessage::Binary(handler::authorize::authorize(login,
                                                                                    private_key,
                                                                                    password)
                    .unwrap()));
        }
        None => {
            println!("null ptr");
        }
    };
}


/// ## Example (C)
/// ### Headers
///
/// ```ignore
/// void skaro_register_type(handle);
/// ```
///
/// ### Implementation
///
/// ```ignore
/// skaro_register_type(x);
/// ```
#[no_mangle]
pub extern "C" fn skaro_register_type(
        handle: *mut std::sync::mpsc::Sender<websocket::OwnedMessage>
    ) {

    let maybe_channel = unsafe { handle.as_mut() };

    match maybe_channel {
        Some(s) => {
            let _ = s.send(websocket::OwnedMessage::Binary(handler::register_type::register_type()
                    .unwrap()));
        }
        None => {
            println!("null ptr");
        }
    };
}

/// ## Example (C)
/// ### Headers
///
/// ```ignore
/// void skaro_register(handle, const char*, const char*, const char*);
/// ```
///
/// ### Implementation
///
/// ```ignore
/// skaro_register(x, "login@domain.org", "login", "password");
/// ```
#[no_mangle]
pub extern "C" fn skaro_register(handle: *mut std::sync::mpsc::Sender<websocket::OwnedMessage>,
                                 c_email: *const c_char,
                                 c_login: *const c_char,
                                 c_password: *const c_char) {

    let maybe_channel = unsafe { handle.as_mut() };

    match maybe_channel {
        Some(s) => {
            let email: &str = unsafe { CStr::from_ptr(c_email) }.to_str().unwrap();
            let login: &str = unsafe { CStr::from_ptr(c_login) }.to_str().unwrap();
            let password: &str = unsafe { CStr::from_ptr(c_password) }.to_str().unwrap();

            let _ = s.send(websocket::OwnedMessage::Binary(handler::register::register(email,
                                                                                       login,
                                                                                       password)
                .unwrap()));
        }
        None => {
            println!("null ptr");
        }
    };
}

/// ## Example (C)
/// ### Headers
///
/// ```ignore
/// void skaro_session_restore(handle, const char*);
/// ```
///
/// ### Implementation
///
/// ```ignore
/// skaro_session_restore(x, "3ce5d8a3-9514-4df3-a796-edff9b382108");
/// ```
#[no_mangle]
pub extern "C" fn skaro_session_restore(
        handle: *mut std::sync::mpsc::Sender<websocket::OwnedMessage>,
        c_session_id: *const c_char
    ) {

    let maybe_channel = unsafe { handle.as_mut() };

    match maybe_channel {
        Some(s) => {
            let session_id: &str = unsafe { CStr::from_ptr(c_session_id) }.to_str().unwrap();

            let _ = s.send(websocket::OwnedMessage::Binary(
                ::handler::session_restore::session_restore(session_id)
                .unwrap()));
        }
        None => {
            println!("null ptr");
        }
    };
}

/// ## Example (C)
/// ### Headers
///
/// ```ignore
/// void skaro_get_contacts(handle);
/// ```
///
/// ### Implementation
///
/// ```ignore
/// skaro_get_contacts(x);
/// ```
#[no_mangle]
pub extern "C" fn skaro_get_contacts(
    handle: *mut std::sync::mpsc::Sender<websocket::OwnedMessage>
) {

    let maybe_channel = unsafe { handle.as_mut() };

    match maybe_channel {
        Some(s) => {
            let _ = s.send(websocket::OwnedMessage::Binary(handler::get_contacts::get_contacts()
                .unwrap()));
        }
        None => {
            println!("null ptr");
        }
    };
}

/// ## Example (C)
/// ### Headers
///
/// ```ignore
/// void skaro_add_contact(handle, const char*);
/// ```
///
/// ### Implementation
///
/// ```ignore
/// skaro_add_contact(x, "user");
/// ```
#[no_mangle]
pub extern "C" fn skaro_add_contact(handle: *mut std::sync::mpsc::Sender<websocket::OwnedMessage>,
                                    c_login: *const c_char) {

    let maybe_channel = unsafe { handle.as_mut() };

    match maybe_channel {
        Some(s) => {
            let login: &str = unsafe { CStr::from_ptr(c_login) }.to_str().unwrap();

            let _ = s.send(websocket::OwnedMessage::Binary(
                ::handler::add_contact::add_contact(login)
                .unwrap()));
        }
        None => {
            println!("null ptr");
        }
    };
}

/// ## Example (C)
/// ### Headers
///
/// ```ignore
/// void skaro_del_contact(handle, const char*);
/// ```
///
/// ### Implementation
///
/// ```ignore
/// skaro_del_contact(x, "user");
/// ```
#[no_mangle]
pub extern "C" fn skaro_del_contact(handle: *mut std::sync::mpsc::Sender<websocket::OwnedMessage>,
                                    c_login: *const c_char) {

    let maybe_channel = unsafe { handle.as_mut() };

    match maybe_channel {
        Some(s) => {
            let login: &str = unsafe { CStr::from_ptr(c_login) }.to_str().unwrap();

            let _ = s.send(websocket::OwnedMessage::Binary(
                ::handler::del_contact::del_contact(login)
                .unwrap()));
        }
        None => {
            println!("null ptr");
        }
    };
}

/// ## Example (C)
/// ### Headers
///
/// ```ignore
/// void skaro_search_contacts(handle, const char*);
/// ```
///
/// ### Implementation
///
/// ```ignore
/// skaro_search_contacts(x, "logi");
/// ```
#[no_mangle]
pub extern "C" fn skaro_search_contacts(
    handle: *mut std::sync::mpsc::Sender<websocket::OwnedMessage>,
    c_query: *const c_char
) {

    let maybe_channel = unsafe { handle.as_mut() };

    match maybe_channel {
        Some(s) => {
            let query: &str = unsafe { CStr::from_ptr(c_query) }.to_str().unwrap();

            let _ = s.send(websocket::OwnedMessage::Binary(
                ::handler::search_contacts::search_contacts(query)
                .unwrap()));
        }
        None => {
            println!("null ptr");
        }
    };
}

/// ## Example (C)
/// ### Headers
///
/// ```ignore
/// void skaro_get_user_data(handle, const char*);
/// ```
///
/// ### Implementation
///
/// ```ignore
/// skaro_get_user_data(x, "login");
/// ```
#[no_mangle]
pub extern "C" fn skaro_get_user_data(
    handle: *mut std::sync::mpsc::Sender<websocket::OwnedMessage>,
    c_login: *const c_char
) {

    let maybe_channel = unsafe { handle.as_mut() };

    match maybe_channel {
        Some(s) => {
            let login: &str = unsafe { CStr::from_ptr(c_login) }.to_str().unwrap();

            let _ = s.send(websocket::OwnedMessage::Binary(
                ::handler::get_user_data::get_user_data(login)
                    .unwrap()));
        }
        None => {
            println!("null ptr");
        }
    };
}

/// ## Example (C)
/// ### Headers
///
/// ```ignore
/// void skaro_get_history(handle, const char*, bool);
/// ```
///
/// ### Implementation
///
/// ```ignore
/// skaro_get_history(x, "login", false);
/// ```
#[no_mangle]
pub extern "C" fn skaro_get_history(handle: *mut std::sync::mpsc::Sender<websocket::OwnedMessage>,
                                    c_login: *const c_char) {

    let maybe_channel = unsafe { handle.as_mut() };

    match maybe_channel {
        Some(s) => {
            let login: &str = unsafe { CStr::from_ptr(c_login) }.to_str().unwrap();

            let _ = s.send(websocket::OwnedMessage::Binary(
                ::handler::get_history::get_history(login).unwrap()));
        }
        None => {
            println!("null ptr");
        }
    };
}

/// The fifth argument specifies the UUID string to be returned when the server responds.
///
/// ## Example (C)
/// ### Headers
///
/// ```ignore
/// void skaro_send_message(handle, const char*, const char*, bool, const char*);
/// ```
///
/// ### Implementation
///
/// ```ignore
/// skaro_send_message(x, "login", "message", false, "23bccb5a-1433-4429-ad60-93a33393b82e");
/// ```
#[no_mangle]
pub extern "C" fn skaro_send_message(
    handle: *mut std::sync::mpsc::Sender<websocket::OwnedMessage>,
    c_to: *const c_char,
    c_body: *const c_char,
    is_conference: bool,
    c_marker: *const c_char
) {

    let maybe_channel = unsafe { handle.as_mut() };

    match maybe_channel {
        Some(s) => {
            let to: &str = unsafe { CStr::from_ptr(c_to) }.to_str().unwrap();
            let body: &str = unsafe { CStr::from_ptr(c_body) }.to_str().unwrap();
            let marker: &str = unsafe { CStr::from_ptr(c_marker) }.to_str().unwrap();

            let _ = s.send(websocket::OwnedMessage::Binary(
                ::handler::send_message::send_message(to, body, is_conference, marker)
                    .unwrap()));
        }
        None => {
            println!("null ptr");
        }
    };
}

/// ## Example (C)
/// ### Headers
///
/// ```ignore
/// void skaro_update_my_data(handle, const char*);
/// ```
///
/// ### Implementation
///
/// ```ignore
/// skaro_update_my_data(x, "{\"login\": \"newlogin\"}");
/// ```
#[no_mangle]
pub extern "C" fn skaro_update_my_data(
    handle: *mut std::sync::mpsc::Sender<websocket::OwnedMessage>,
    c_records: *const c_char
) {

    let maybe_channel = unsafe { handle.as_mut() };

    match maybe_channel {
        Some(s) => {
            let records: &str = unsafe { CStr::from_ptr(c_records) }.to_str().unwrap();

            let _ = s.send(websocket::OwnedMessage::Binary(
                handler::update_my_data::update_my_data(records)
                    .unwrap()));
        }
        None => {
            println!("null ptr");
        }
    };
}
