use std::collections::HashMap;
use serde_cbor::{Value, ObjectKey};
use serde_json;

#[derive(Debug, Clone)]
pub struct SkaroMessage<'a>(pub &'a HashMap<ObjectKey, Value>);
#[derive(Debug, Clone)]
pub struct SkaroVecMessage<'a>(pub &'a Vec<Value>);

impl<'a> SkaroMessage<'a> {
    pub fn get_int<'b>(&self, key: &'b str) -> Result<i32, String> {
        match self.0.get(&ObjectKey::String(key.to_string())) {
            Some(&Value::I64(i)) => Ok(i as i32),
            _ => Err(format!("Key '{}' could not be found", key)),
        }
    }
    pub fn get_str<'b>(&self, key: &'b str) -> Result<&'a str, String> {
        match self.0.get(&ObjectKey::String(key.to_string())) {
            Some(&Value::String(ref s)) => Ok(s.as_str()),
            _ => Err(format!("Key '{}' could not be found", key)),
        }
    }
    pub fn get_obj<'b>(&self, key: &'b str) -> Result<SkaroMessage, String> {
        match self.0.get(&ObjectKey::String(key.to_string())) {
            Some(&Value::Object(ref s)) => Ok(SkaroMessage(s)),
            _ => Err(format!("Key '{}' could not be found", key)),
        }
    }
    pub fn get_arr<'b>(&self, key: &'b str) -> Result<SkaroVecMessage, String> {
        match self.0.get(&ObjectKey::String(key.to_string())) {
            Some(&Value::Array(ref s)) => Ok(SkaroVecMessage(s)),
            _ => Err(format!("Key '{}' could not be found", key)),
        }
    }
    pub fn to_json(&self) -> Result<String, String> {
        let j = serde_json::to_string(self.0).unwrap();
        Ok(j)
    }
}

impl<'a> SkaroVecMessage<'a> {
    pub fn to_json(&self) -> Result<String, String> {
        let j = serde_json::to_string(self.0).unwrap();
        Ok(j)
    }
}
