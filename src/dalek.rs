extern crate websocket;
extern crate serde_cbor;
extern crate openssl;
extern crate base64;
extern crate sha;

pub fn ws_connect(connection: &str) -> *mut () {
    use std::thread;
    use std::sync::mpsc::channel;

    use websocket::{Message, OwnedMessage};
    use websocket::client::ClientBuilder;

    println!("Connecting to {}", connection);

    let client = ClientBuilder::new(connection)
        .unwrap()
        .add_protocol("cbor-typed")
        .connect_insecure()
        .unwrap();

    println!("Successfully connected");

    let (mut receiver, mut sender) = client.split().unwrap();

    let (tx, rx) = channel();

    let tx_1 = tx.clone();
    let tx_2 = tx.clone();

    thread::spawn(move || {
        loop {
            // Send loop
            let message = match rx.recv() {
                Ok(m) => m,
                Err(e) => {
                    println!("Send Loop: {:?}", e);
                    return;
                }
            };
            match message {
                OwnedMessage::Close(_) => {
                    let _ = sender.send_message(&message);
                    // If it's a close message, just send it and then return.
                    return;
                }
                _ => (),
            }
            // Send the message
            match sender.send_message(&message) {
                Ok(()) => (),
                Err(e) => {
                    println!("Send Loop: {:?}", e);
                    let _ = sender.send_message(&Message::close());
                    return;
                }
            }
        }
    });

    thread::spawn(move || {
        // Receive loop
        for message in receiver.incoming_messages() {
            let message = match message {
                Ok(m) => m,
                Err(e) => {
                    println!("Receive Loop Error: {:?}", e);
                    let _ = tx_1.send(OwnedMessage::Close(None));
                    return;
                }
            };

            let message = ::parse::cbor(message);
            match message {
                None => println!("Отвечать не нужно"),
                Some(data) => {
                    println!("Отвечаем: {:?}", data);
                    let _ = tx_2.send(OwnedMessage::Binary(data.unwrap()));
                }
            }

        }
    });

    let context = Box::new(tx);
    let handle = Box::into_raw(context) as *mut ();

    handle

}
