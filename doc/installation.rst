Installation
============

libskaro requires `Python`_ >= 3.5 and is known to work on Linux, Mac
OS X, and Windows (with `Cygwin`_).

.. _Python: https://www.python.org/
.. _Cygwin: http://cygwin.com/

Python Package Index (PyPI)
---------------------------

libskaro is listed in `PyPI`_, and may be installed using `pip`_::

  pip3 install libskaro

.. _PyPI: https://pypi.python.org/pypi/hangups
.. _pip: https://pip.pypa.io/

Install from Source
-------------------

The libskaro code is available from GitLab. Either download and extract a
`libskaro release archive`_, or clone the `libskaro repository`_::

  git clone https://gitlab.com/exarh-team/libskaro.git

Switch to the hangups directory and install the package::

  cd hangups
  python3 setup.py install

.. _libskaro release archive: https://gitlab.com/exarh-team/libskaro/repository/archive.zip?ref=master
.. _libskaro repository: https://gitlab.com/exarh-team/libskaro.git

