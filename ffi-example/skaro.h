typedef void * handle;
extern handle skaro_connect(const char*);

void skaro_authorize(handle, const char*, const char*, const char*);
void skaro_register(handle, const char*, const char*, const char*);
void skaro_disconnect(handle);

void skaro_get_contacts(handle);
void skaro_add_contact(handle, const char*);
void skaro_del_contact(handle, const char*);
void skaro_search_contacts(handle, const char*);
void skaro_get_user_data(handle, const char*);
