#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "skaro.h"

void trigger_callback() {
  printf("trigger_callback!");fflush(stdout);
}

void skaro_export_keys(const char* public_key, const char* private_key) {
    printf("export_keys!");
    printf(public_key);
    printf(private_key);

    FILE *fp;
    size_t count;

    fp = fopen("private.key", "wb");
    if(fp == NULL) {
        perror("ошибка открытия пример.txt");
        return;
    }
    count = fwrite(private_key, strlen(private_key), 1, fp);
    printf("Записано %lu байт. fclose(fp) %s.\n", (unsigned long)count, fclose(fp) == 0 ? "успешно" : "с ошибкой");

    fflush(stdout);
}

void skaro_on_authorize(const char* status, const char* info, const char* data) {
    printf("skaro_on_authorize");
    printf(status);
    printf(info);
    printf(data);
}

void skaro_on_register_type(const char* status, const char* info, const char* register_type){}

void skaro_on_register(const char* status, const char* info) {
    printf("skaro_on_register");
    printf(status);
    printf(info);
}

void skaro_on_session_restore(const char* status, const char* info, const char* session_id, const char* data) {}

void skaro_on_get_contacts(const char* status, const char* info, const char* data) {}

void skaro_on_add_contact(const char* status, const char* info, const char* data) {}

void skaro_on_del_contact(const char* status, const char* info) {}

void skaro_on_search_contacts(const char* status, const char* info, const char* data) {}

void skaro_on_get_user_data(const char* status, const char* info, const char* data) {}

void skaro_on_changed_user_data(const char* status, const char* info, const char* data) {}

void skaro_on_get_history(const char* status, const char* info, const char* data) {}

void skaro_on_send_message(const char* status, const char* info, int mid, const char* date) {}

void skaro_on_received_message(const char* status, const char* info, int mid, const char* date, const char* from, const char* body) {}

void skaro_on_update_my_data(const char* status, const char* info) {}

void skaro_on_error(const char* status, const char* info) {}

void skaro_on_info(const char* status, const char* info) {}

int main()
{
    int c;

    printf("Hello, world!");
    fflush(stdout);
    
    handle x;
    x = skaro_connect("ws://127.0.0.1:8000/skaro_client/");
        printf(x);fflush(stdout);
    fflush(stdout);
    
    
    c = getchar( );
    
    skaro_register(x, "modtest@modtest7.tt", "modtest7", "mypass");

    c = getchar( );






    FILE* f;
    // открываем файл
    if((f = fopen("private.key", "rb")) == NULL){
        printf("Error opening file 'in'");
        return 0;
    }

    // узнаем размер файла для создания буфера нужного размера
    fseek(f, 0L, SEEK_END);
    long size = ftell(f);
    fseek(f, 0L, SEEK_SET);

    // выделяем память под буфер
    char* buf = (char*)malloc(sizeof(char) * size);
    // читаем полностью весь файл в буфер
    fread(buf, 1, size, f);

    // выводим содержимое буфера в стандартный поток
    fprintf(stdout, "%s", buf);

    fclose(f);








    skaro_authorize(x, "modtest7", buf, "mypass");

    c = getchar( );

    skaro_disconnect(x);

    c = getchar( );
    return 0;
}
